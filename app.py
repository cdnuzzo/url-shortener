from flask import Flask, render_template, request, redirect, url_for, flash, abort, session, jsonify
import json, os.path
from werkzeug.utils import secure_filename

app = Flask(__name__)
app.secret_key = 'adl3943948ahdjh3ii349039dhdhdd2'


@app.route('/')
def home():
    return render_template('home.html', codes=session.keys())


@app.route('/your-url', methods=['GET', 'POST'])
def about():
    if request.method == 'POST':
        # create a dict
        urls = {}

        # checks if urls.json exists and if so adds entry to dict
        if os.path.exists('urls.json'):
            with open('urls.json') as urls_file:
                urls = json.load(urls_file)

        # checks if a key already exists before saving it
        if request.form['code'] in urls.keys():
            flash('That short name has already been taken. Please select another name.')
            return redirect(url_for('home'))

        # check if file or if url, do something depending on which
        if 'url' in request.form.keys():
            # specify our key and assign to url from form entry
            urls[request.form['code']] = {'url':request.form['url']}
        else:
            # upload our file and save it to disk
            f = request.files['file']
            full_name = request.form['code'] + secure_filename(f.filename)
            f.save('/home/craig/url-shortener/static/user_files/' + full_name)
            urls[request.form['code']] = {'file':full_name}

        # writes url and short name to a json file
        with open('urls.json', 'w') as url_file:
            json.dump(urls, url_file)
            session[request.form['code']] = True
        return render_template('your_url.html', code=request.form['code'])
    else:
        return redirect(url_for('home'))

# redirect based on string provided that matches the dict value saved in the json file
@app.route('/<string:code>')
def redirect_to_url(code):
    if os.path.exists('urls.json'):
        with open('urls.json') as urls_file:
            urls = json.load(urls_file)
            if code in urls.keys():
                if 'url' in urls[code].keys():
                    return redirect(urls[code]['url'])
                # looking for a file in the else
                else:
                    return redirect(url_for('static', filename='user_files/'+ urls[code]['file']))

    return abort(404)


@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404


@app.route('/api')
def session_api():
    return jsonify(list(session.keys()))